# Collabprint

Collabprint is a fork of [ether2html](http://osp.kitchen/tools/ether2html/) made by Open Source Publishing.

*Working document in the browser*

![](http://osp.kitchen/api/http://osp.kitchen/api/osp.tools.ether2html/45f025c0b37efd149d4dfdb9ce0af2d01ff5ff81/blob-data/book-screengrab.png)

# How to use it
0. Install MAMP, go in the application, you will find an htdocs file. Put the folder downloaded in that folder. To begin, Launch MAMP and Start the webserver. 
1. Create an etherpad somewhere for the CSS (e.g. <https://framapad.org>).
2. Create an etherpad somewhere for the content (e.g. <https://framapad.org>).
3. Click on the link [index.html] and the [print.html] then download the file by right-click "Download raw" and choose "Save link as".
4. Edit that index.html] and the [print.html] file by replacing the URL under the comment `<!-- CHANGE THE URL OF YOUR CSS PAD BELOW -->` by the export URL of the pad CSS you created in step 1, copy the link location of the plain text export of the pad - see below the screen capture.
5. Edit that index.html] and the [print.html] file by replacing the URL under the comment `<!-- CHANGE THE URL OF YOUR MARKDOWN CONTENT PAD BELOW -->` by the export URL of the pad for the content you created in step 2, copy the link location of the plain text export of the pad - see below the screen capture.
6. Open the file index.html] and the [print.html] in your web browser (Firefox or Chrome).
7. Edit your pad with content (markdown or html) and your pad with CSS styles.
8. Reload the file index.html] and the [print.html] opened in your web browser.
9. Use the print function of your browser and choose "Save as file". Here is your pdf ready to print!

*Copy the link location of your pad*

![](http://osp.kitchen/api/osp.tools.ether2html/b72c830156ff61069478b0052f9d77b52affa539/blob-data/pad-menu-screengrab.png)

# HTML

HTML (*Hypertext Markup Language*) is the standard markup language for Web pages. With HTML you can create your own Website.

Remember :

## Headings :
<h1>Big Title</h1>
<h2>Subtitle</h2>
<h3>Sub-subtitle</h3>
<h4>...</h4>
<h5>...</h5>
<h6>...</h6>

## Paragraphs and Tags :
<p>Creates a new paragraph</p>
<b>Creates bold text (should use <strong> instead)</b>
<i>Creates italicized text (should use <em> instead)</i>
<em>Emphasizes a word (usually processed in italics)</em>
<strong>Emphasizes a word (usually processed in bold)</strong>

## lists
Unorderate list :
<ul>
  <li>first line</li>
  <li>second line</li>
</ul>

<ol>
  <li>first line</li>
  <li>second line</li>
</ol>

## Links and images :
<a href="URL">clickable text</a>
<figure>
  <img src="URL">
  <figcaption>Caption images</figcaption>
</figure>

There is a [link](https://www.codecademy.com/learn/learn-html/modules/learn-html-elements/cheatsheet) to go further.

# CSS

Cascading Style Sheets is a style sheet language used for describing the presentation of a document written in a markup language such as HTML. CSS is a cornerstone technology of the World Wide Web, alongside HTML and JavaScript.

To go [further](https://www.w3schools.com/Css/default.asp).



# Paged.js

For these workshop we using the [Paged.js](https://www.pagedjs.org/) polyfill. Paged.js is a free and open source JavaScript library that paginates content in the browser to create PDF output from any HTML content. This means you can design works for print (eg. books) using HTML and CSS!

You must read the [documentation](https://www.pagedjs.org/documentation/), if you stuck by CSS print rules.

# Licence

© Quentin Juhel under the GNU-GPL3
